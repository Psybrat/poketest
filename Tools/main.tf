provider "aws" {
	region= "eu-central-1"
}

locals {
	AWS_SSH_KEY_NAME = "ssh-key-frankfurt"
	PROJECT_NAME = "sberpipe"
}


terraform {
	backend "s3" {
		bucket = "sberpipe"
		key = "infrastructure/terraform.tfstate"
		region = "eu-central-1"
	}
}


data "aws_ami" "latest_ubuntu" {
	owners = ["099720109477"]
	most_recent = true
	filter {
		name = "name"
		values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
	}
}

data "aws_ami" "latest_RHEL" {
	owners = ["309956199498"]
	most_recent = true
	filter {
		name = "name"
		//values = ["RHEL-7.1_HVM-20150803-x86_64-1-Hourly2-GP2"]
		values = ["RHEL_HA-8.4.0_HVM-*-x86_64-2-Hourly2-GP2"]
	}
}



resource "aws_instance" "jenkins_master" {
	ami = data.aws_ami.latest_ubuntu.id
	instance_type = "t2.medium"
	user_data = file("./ec2_init_scripts/jenkins_instance_init.sh")
	key_name = local.AWS_SSH_KEY_NAME
	security_groups = [aws_security_group.jenkins.name]
	root_block_device {
		volume_size = 16
	}
	tags = {
		Name = "jenkins"
		Project = local.PROJECT_NAME
	}
}


resource "aws_instance" "nexus" {
	ami = data.aws_ami.latest_ubuntu.id
	instance_type = "t2.large"
	user_data = file("./ec2_init_scripts/nexus_instance_init.sh")
	key_name = local.AWS_SSH_KEY_NAME
	security_groups = [aws_security_group.nexus.name]
	root_block_device {
		volume_size = 16
	}
	tags = {
		Name = "nexus"
		Project = local.PROJECT_NAME
	}
}


resource "aws_instance" "openshift" {
    ami = data.aws_ami.latest_RHEL.id
		instance_type = "t2.medium"
		key_name = local.AWS_SSH_KEY_NAME
		security_groups = [aws_security_group.openshift.name]
		root_block_device {
			volume_size = 25
		}
		tags = {
			Name = "openshift"
			Project = local.PROJECT_NAME
		}
}

resource "aws_security_group" "openshift" {

	name = "Openshitf sequrity group"
	dynamic "ingress" {
		for_each = ["80", "22", "443", "2376", "8443"]
		content {
			from_port = ingress.value
			to_port = ingress.value
			protocol = "tcp"
			cidr_blocks = ["0.0.0.0/0"]
		}
	}

	egress {
		from_port = 0
		to_port = 0
		protocol = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
}


resource "aws_security_group" "jenkins" {

	name = "Jenkins sequrity group"
	dynamic "ingress" {
		for_each = ["8080", "22", "50000"]
		content {
			from_port = ingress.value
			to_port = ingress.value
			protocol = "tcp"
			cidr_blocks = ["0.0.0.0/0"]
		}
	}

	egress {
		from_port = 0
		to_port = 0
		protocol = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
}

resource "aws_security_group" "nexus" {
	name = "Nexus Security group"

	dynamic "ingress" {
		for_each = ["8081", "8082", "22"]
		content {
			from_port = ingress.value
			to_port = ingress.value
			protocol = "tcp"
			cidr_blocks = ["0.0.0.0/0"]
		}
	}

	egress {
		from_port = 0
		to_port = 0
		protocol = -1
		cidr_blocks = ["0.0.0.0/0"]
	}
}
