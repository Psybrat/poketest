output "jenkins_public_ip" {
  description = "Public IP Jenkins instance"
  value = aws_instance.jenkins_master.public_ip
}

output "nexus_public_ip" {
  description = "Public IP nexus instance"
  value = aws_instance.nexus.public_ip
}
