FROM python:3.8-alpine
ADD requirements.txt .

RUN apk update
RUN apk add --no-cache git gcc g++ libffi-dev musl-dev zlib-dev pcre pcre-dev openssl-dev build-base \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del git gcc g++ libffi-dev musl-dev postgresql-dev zlib-dev pcre-dev build-base

RUN addgroup -S pokemon && adduser -S pokemon -G pokemon
USER pokemon

EXPOSE 8000/tcp
ADD app ./app/

WORKDIR /app

#ADD Tools/config/prestart.sh .
#CMD ["./prestart.sh"]

ENTRYPOINT ["gunicorn", "--bind=0.0.0.0:8000", "--workers=1", "pokemon.wsgi:application", "--worker-tmp-dir", "/tmp-gunicorn"]
