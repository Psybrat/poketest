### Краткое описание

Учебный проект по созданию CI/CD pipe. 

### Стек 

 - python (Django) 
 - jenkins
 - docker 
 - nexus
 - openshift
 - terraform

### ToDo

-   Инфраструктура;
    -   provider: aws
    -   tool: terraform 
    -   composition:
        - 2 EC2 Ubuntu Servers
        - 1 EC2 RHEL Server 
        - 3 security groups
        
-   1 Jenkins Master
    - pools the bitbucket repo
    - takes code from bitbucket 
    - creates docker image
    - pushes docker image to nexus repo
-   2 Nexus 
    - takes an docker image 
-   3 Openshift cluster (not realized) 
    - takes docker image from Nexus
    - deploy it on self nodes
    

### TO USE

-  preparation:
    - clone project 
    - create encrypted private bucket on any aws region (remote terraform state)
    - create aws ssh keys (ssh connections to instances) 
    - change it in `/Tools/main.tf`
   
-  create infrastructure:
    - initialize terraform in `Tools/`
    <terraform init>
    - apply 
    <terraform apply>
#### Servers
-  nexus:
    - create deployment User
    - create docker stored repository 
  
-  jenkins master
    - initialize it
    - config credentials to nexus and bitbucket 
    - create job with `Tools/docker/jenkins_docker.jenkins` pipeline 
   
-  openshift 
    - soon
   
    
























 







   
